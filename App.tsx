import React, { useEffect, useState } from 'react';
import './App.css';
import axios from 'axios';
import moment from 'moment';
import { Button, Container, Card, Row } from 'react-bootstrap';


const App = () => {
  type IKeys = { date: string };
  const initialDate = moment("2020-03-13").format("YYYY-MM-DD");
  const [userInputDate, setUserInputDate] = useState<IKeys>({ date: initialDate });

  type ResponseData = { earth: { x: number, y: number, z: number } };
  const [coordinates, setCoordinates] = useState<ResponseData>();

  const validateUserInputDate = (value: string) => {
    try {
      const newDate = moment(value).format("YYYY-MM-DD");
      setUserInputDate({ date: newDate });
    } catch (err) { }

  }
  const getCoords = () => {

    axios.post("http://localhost:3001/planets/coordinates", {
      date: userInputDate.date
    })
      .then(function (response) {
        console.log('reponse data from POST resource: ', response.data.earth);
        setCoordinates({ earth: response.data.earth });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  useEffect(() => {
    getCoords();
  },
    []
  );

  return (
    <div className='App'>
      <h1>React Application</h1>
      <div className='form'>
        <input name='date' placeholder='Enter date eg. 2000-05-25' onChange={(e) => { validateUserInputDate(e.target.value) }} />
      </div>
      <Button className='my-2' variant="primary" onClick={() => getCoords()} >Submit</Button> <br /><br />
      <Container>
        <Row>
          <Card>On given date {userInputDate.date} the Earth coordinates: {coordinates?.earth?.x}, {coordinates?.earth?.y}, {coordinates?.earth?.z}</Card>
        </Row>
      </Container>
    </div>
  );


}

export default App;


