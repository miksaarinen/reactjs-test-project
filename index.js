import express, { json, urlencoded } from 'express';
import moment from 'moment';
import cors from 'cors';

const app = express();
app.use(cors());
app.use(json());
app.use(urlencoded({ extended: true }));

app.post("/planets/coordinates", (req, res) => {
  try {
    const dateInput = moment(req.body.date).format("YYYY-MM-DD");
    res.send({ time: dateInput, earth: { x: "100", y: "40", z: "125" } })

  } catch (err) {}
})

app.listen('3001', () => { })